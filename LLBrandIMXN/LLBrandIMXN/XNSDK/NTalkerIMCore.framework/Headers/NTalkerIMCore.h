//
//  NTalkerIMCore.h
//  NTalkerIMCore
//
//  Created by Lee on 2018/3/13.
//  Copyright © 2018年 NTalker. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NTalkerIMCore.
FOUNDATION_EXPORT double NTalkerIMCoreVersionNumber;

//! Project version string for NTalkerIMCore.
FOUNDATION_EXPORT const unsigned char NTalkerIMCoreVersionString[];
// In this header, you should import all the public headers of your framework using statements like #import <NTalkerIMCore/PublicHeader.h>
#import <NTalkerIMCore/NtalkerSDKCore.h>
#import <NTalkerIMCore/NTalkerChatTableHeaderModel.h>
#import <NTalkerIMCore/NTalkerExtension.h>
#import <NTalkerIMCore/NtalkerCoreMessage.h>
#import <NTalkerIMCore/NtalkerUserInfo.h>
#import <NTalkerIMCore/NtalkerConfigureInfo.h>
#import <NTalkerIMCore/MessageType.h>
#import <NTalkerIMCore/NtalkerEventModel.h>
#import <NTalkerIMCore/NtalkerLeaveMsgSetInfoModel.h>
#import <NTalkerIMCore/NtalkerStorageManager.h>
#import <NTalkerIMCore/NtalkerDBManager.h>
#import <NTalkerIMCore/NtalkerCoreConstant.h>
#import <NTalkerIMCore/NtalkerUtilityHelper.h>
#import <NTalkerIMCore/NtalkerLeaveMsgDetailModel.h>
#import <NTalkerIMCore/NtalkerHTTPTool.h>
#import <NTalkerIMCore/NtalkerTemplateRecord.h>
#import <NTalkerIMCore/NtalkerHTTPReportPushStatusTool.h>


