//
//  NtalkerChatParams.h
//  NTalkerGuestIMKit
//
//  Created by wzh on 2018/4/11.
//  Copyright © 2018年 NTalker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NtalkerChatDelegate.h"

@interface NtalkerChatParams : NSObject
///接待组ID（必选参数）
@property (nonatomic, copy) NSString *settingId;
///产品ID
@property (nonatomic, copy) NSString *productId;
/// 页面等级
@property (nonatomic, copy) NSString *pagelevel;
///聊天页面代理（选填，“如果需要监听聊天页面事件时必须设置代理”）
@property (nonatomic, assign) id<NtalkerChatDelegate> delegate;
@end
