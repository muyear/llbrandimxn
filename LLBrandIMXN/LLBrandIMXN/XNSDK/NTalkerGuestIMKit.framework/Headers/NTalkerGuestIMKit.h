//
//  NTalkerGuestIMKit.h
//  NTalkerGuestIMKit
//
//  Created by Lee on 2018/3/13.
//  Copyright © 2018年 NTalker. All rights reserved.
//

#import <UIKit/UIKit.h>


//! Project version number for NTalkerGuestIMKit.
FOUNDATION_EXPORT double NTalkerGuestIMKitVersionNumber;

//! Project version string for NTalkerGuestIMKit.
FOUNDATION_EXPORT const unsigned char NTalkerGuestIMKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NTalkerGuestIMKit/PublicHeader.h>
#import <NTalkerGuestIMKit/Ntalker.h>
#import <NTalkerGuestIMKit/NtalkerChatDelegate.h>
#import <NTalkerGuestIMKit/NtalkerTrailModel.h>
#import <NTalkerGuestIMKit/NTalkerLeaveMessageController.h>
#import <NTalkerGuestIMKit/NtalkerChatParams.h>
#import <NTalkerGuestIMKit/NtalkerConsultRecord.h>
