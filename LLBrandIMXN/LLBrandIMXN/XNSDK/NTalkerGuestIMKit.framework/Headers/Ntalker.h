//
//  Ntalker.h
//  NtalkerIMKit
//
//  Created by wzh on 2017/12/20.
//  Copyright © 2017年 Ntalker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

///未读消息通知
/*未读消息数据格式
 @{@"settingId": settingId , @"unreadCount" :@(unreadCount), @"userName": userName,@"msgContent": @"你好啊", @"msgTime": 时间戳}
 */
extern NSString *const NtalkerUnReadMessageNotification;

@class NtalkerChatParams,NtalkerTrailModel,NtalkerConsultRecord;

@interface Ntalker : NSObject

#pragma mark -  内部使用
/**
 获取当前企业的客服组信息
 
 @param success 成功回调
 @param failure 失败回调
 */
+ (void)ntalker_getSDKTemplateSuccess:(void(^)(NSArray<NSDictionary *> *templateArray))success failure:(void(^)(NSError *error))failure;


#pragma mark -  API
/**
 初始化SDK（公版用户）
 
 @param siteid 企业ID
 @param completion 初始化结果回调
 */

+ (void)ntalker_initSDKWithSiteid:(NSString *)siteid completion:(void (^)(BOOL finished , NSUInteger code))completion;

/**
 初始化SDK(独立部署客户专用)

 @param siteid 企业ID
 @param serverAddress 独立部署的服务器地址
 @param completion 初始化结果回调
 */
+ (void)ntalker_initSDKWithSiteid:(NSString *)siteid flashserverAddress:(NSString *)serverAddress completion:(void (^)(BOOL finished , NSUInteger code))completion;

/**
 初始化一个聊天窗口实体

 @param chatParams 创建聊天实体需要的参数
 @return 聊天窗口实体
 */
+ (UIViewController *)ntalker_chatViewControllerWithChatParam:(NtalkerChatParams *)chatParams;

/**
 用户登录
 
 @param userId 必填,登录用户的id, 只能输入数字、英文字母和"@"、"."、"_"、"-"三种字符，长度小于等于60位。
 @param userName 非必填,登录用户名, 如未填写,系统随机会随机生成一个用户名
 @return 参数判断的返回值,0为参数正确
 */
+ (NSInteger)ntalker_loginWithUserId:(NSString *)userId andUserName:(NSString *)userName;

/**
 用户登录
 
 @param userId 必填,登录用户的id, 只能输入数字、英文字母和"@"、"."、"_"、"-"三种字符，长度小于等于60位。
 @param userName 非必填,登录用户名, 如未填写,系统随机会随机生成一个用户名
 @param userLevel 用户级别
 @return 参数判断的返回值,0为参数正确
 */
+ (NSInteger)ntalker_loginWithUserId:(NSString *)userId andUsername:(NSString *)userName andUserLevel:(NSString *)userLevel;
/**
 用户登出
 */
+ (void)ntalker_logout;

/**
 上报轨迹
 
 @param pageName 当前页面名称
 @param model 轨迹参数
 */
+ (void)ntalker_uploadTrailWithPageName:(NSString *)pageName model:(NtalkerTrailModel *)model;

/**
 日志开关
 
 @param enableLog 是否开启
 */
+ (void)ntalker_setLogSwitch:(BOOL)enableLog;

/**
 获取SDK版本号

 @return 版本号
 */
+ (NSString *)ntalker_versionNumber;

/**
 设置主题色（默认主题色是:#0684ea）

 @param themeColor 十六机制颜色
 */
+ (void)ntalker_themeColor:(NSString *)themeColor;


/**
 查询当前用户的历史咨询记录

 @param count 记录条数
 @param offset 偏移量
 @return 查询结果
 */
+ (NSArray <NtalkerConsultRecord *>*)ntalker_getConsultHistoryListCount:(NSInteger)count offSet:(NSInteger)offset;

/**
 删除一条咨询记录

 @param settingId 接待组ID
 */
+ (void)ntalker_deleteConsultBySettingId:(NSString *)settingId;

/**
 将一条咨询记录的消息未读数设置为“0”

 @param templateId 接待组ID
 */
+ (void)ntalker_updateNtalkerTemplateRecordUnReadMsgCountAsZeroByTemplateId:(NSString *)templateId;


/**
 聊天窗口消息加载模式（默认0：手动加载）

 @param pattern 0：手动加载 1：自动加载
 */
+ (void)ntalker_historyMessageLoadingPattern:(NSUInteger)pattern;

/**
 设置IMEI号（如果不设置，默认是SDK生成的设备唯一标示）

 @param imei IMEI号
 */
+ (void)ntalker_setupImei:(NSString *)imei;

/**
 加密方式
 0：不加密（缺省）
 1：RSA
 2：DES
 3：AES
 @param encryption 加密方式
 */
+ (void)ntalker_setEncryption:(NSUInteger)encryption;

/**
 是否开启超媒体吸底功能（默认开启）
 
 @param enableSnap enableSnap
 */
+ (void)ntalker_setEnableSnap:(BOOL)enableSnap;

/**
 设置语音识别APPID
 如果需使用科大讯飞语音识别定制的功能，需要替换科大讯飞的SDK并且必须传入APPID
 @param AppId 科大讯飞SDK APPID
 */
+ (void)ntalker_setASRAppId:(NSString *)AppId;
/**
 是否隐藏聊窗键盘区域预览图片长图、GIF标识

 @param isHidden 是否隐藏(默认显示)
 */
+ (void)ntalker_isHiddenLongImageOrGifLogo:(BOOL)isHidden;


/**
 自定义消息发送接口(目前只支持文本消息发送)

 @param chatController 聊窗实体(必须是小能SDK创建的聊窗实体)
 @param type 消息类型:  11:文本消息 12:图片消息 13:语音消息 14:视频消息
 @param message 消息内容
 */
+ (void)ntalker_sendMessageWithChatController:(UIViewController *)chatController messageType:(NSInteger)type mesaage:(NSString *)message;


/**
 视频客服的分辨率

 @param resolution 分辨率
 */
+ (void)ntalker_videoCallResolution:(NSUInteger)resolution;
@end
