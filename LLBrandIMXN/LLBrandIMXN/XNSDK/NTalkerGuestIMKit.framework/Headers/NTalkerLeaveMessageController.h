//
//  NTalkerLeaveMessageController.h
//  NTalkerGuestIMKit
//
//  Created by wzh on 2018/3/31.
//  Copyright © 2018年 NTalker. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^canceButtonClickBlcok)(void);


@interface NTalkerLeaveMessageController : UITableViewController
@property (nonatomic, copy) NSString *siteId;
@property (nonatomic, copy) canceButtonClickBlcok canceButtonClick;
@property (nonatomic, strong) NSString *fromClass;

+ (void)getLeaveMsgConfigureInfo:(void (^)(BOOL isSuccess))block;

@end
