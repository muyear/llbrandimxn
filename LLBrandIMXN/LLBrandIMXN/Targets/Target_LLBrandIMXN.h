//
//  Target_LLBrandIMXN.h
//  LLBrandIMXN
//
//  Created by muyear on 2018/12/18.
//  Copyright © 2018 muyear. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface Target_LLBrandIMXN : NSObject
- (void)Action_startWithSiteid:(NSDictionary *)params;
- (void)Action_registerPushDeviceID:(NSDictionary *)params;
- (void)Action_handleRemoteNotification:(NSDictionary *)params;
- (void)Action_loginWithUserId:(NSDictionary *)params;
- (void)Action_loginOut:(NSDictionary *)params;
- (UIViewController *)Action_chatViewController:(NSDictionary *)params;
- (void)Action_productTrail:(NSDictionary *)params;
- (void)Action_orderTrail:(NSDictionary *)params;
- (void)Action_unReadCount:(NSDictionary *)params;
@end

NS_ASSUME_NONNULL_END
