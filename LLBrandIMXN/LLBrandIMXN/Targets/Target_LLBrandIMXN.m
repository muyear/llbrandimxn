//
//  Target_LLBrandIMXN.m
//  LLBrandIMXN
//
//  Created by muyear on 2018/12/18.
//  Copyright © 2018 muyear. All rights reserved.
//

#import "Target_LLBrandIMXN.h"
//#import "JPUSHService.h"
#import <NTalkerGuestIMKit/NTalkerGuestIMKit.h>
#import <Masonry/Masonry.h>
#import <NTalkerIMCore/NTalkerChatTableHeaderModel.h>
#import <SDWebImage/UIImageView+WebCache.h>
@interface Target_LLBrandIMXN()<NtalkerChatDelegate>
@property (nonatomic, assign)NSInteger allUnreadCount;
@property (nonatomic, weak)UIViewController *chatVC;
@property (nonatomic, copy)void(^unreadCallback)(NSString * unread);
@end

@implementation Target_LLBrandIMXN
- (void)dealloc {
    
}
- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)Action_startWithSiteid:(NSDictionary *)params {
    NSString *siteId = params[@"siteid"];
    NSString *jPushAppKey = params[@"jPushAppKey"];
    //NSDictionary *launchOptions = params[@"launchOptions"];
    if (!siteId || siteId.length == 0) {
        siteId = @"kf_1002";
    }
    [Ntalker ntalker_initSDKWithSiteid:siteId completion:^(BOOL finished, NSUInteger code) {
        
    }];
    [Ntalker ntalker_setLogSwitch:NO];
    
    //极光推送相关代码
    if (jPushAppKey) {
//        JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
//        entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
//        [JPUSHService setupWithOption:launchOptions appKey:jPushAppKey channel:nil apsForProduction:NO];
//        [JPUSHService registerForRemoteNotificationConfig:entity delegate:nil];
    }
  
}
- (void)Action_registerPushDeviceID:(NSDictionary *)params {
//    NSData *deviceToken = params[@"deviceToken"];
//    [JPUSHService registerDeviceToken:deviceToken];
//    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
//       // [Ntalker ntalker_registerID:registrationID isProduction:NO];
//    }];
}
- (void)Action_handleRemoteNotification:(NSDictionary *)params {
   // NSDictionary *userInfo = params[@"userInfo"];
    //[Ntalker ntalker_handleRemoteNotification:userInfo];
    
}
- (void)Action_loginWithUserId:(NSDictionary *)params {
    NSString *userId = params[@"userId"];
    NSString *userName = params[@"userName"];
    void(^callback)(NSString * loginResult) = params[@"callback"];
    NSInteger result = [Ntalker ntalker_loginWithUserId:userId andUserName:userName];
    if (callback) {
        callback([NSString stringWithFormat:@"%ld",result]);
    }
    
}
- (void)Action_loginOut:(NSDictionary *)params {
    [Ntalker ntalker_logout];
}

- (UIViewController *)Action_chatViewController:(NSDictionary *)params {
    NSString *settingId = params[@"settingId"];
    NSString *productId = params[@"productId"];
    NtalkerChatParams *chatParems = [[NtalkerChatParams alloc] init];
    chatParems.delegate = self;
    chatParems.settingId = settingId;
    chatParems.productId = productId;
    if (self.chatVC) {
        self.chatVC = nil;
    }
    UIViewController *chatVC = [Ntalker ntalker_chatViewControllerWithChatParam:chatParems];
    self.chatVC = chatVC;
    [Ntalker ntalker_historyMessageLoadingPattern:1];
    return chatVC;
}

- (void)Action_productTrail:(NSDictionary *)params {
    NtalkerTrailModel *model = [[NtalkerTrailModel alloc] init];
    model.ttl = @"商品详情";
//    model.lev = @"3";//等级
    model.prid = params[@"productId"];//商品id
    model.pn = params[@"productName"];
    model.mp = params[@"mp"];//市场价格
    model.sp = params[@"sp"];//网站价格
    model.iu = params[@"iu"];//商品图片链接
    [Ntalker ntalker_uploadTrailWithPageName:@"商品页" model:model];
}
- (void)Action_orderTrail:(NSDictionary *)params {
    NtalkerTrailModel *model = [[NtalkerTrailModel alloc] init];
    model.ttl = @"订单详情";//页面标题
//    model.lev = @"5";//等级
    model.oi = params[@"orderId"];//订单id
    model.op = params[@"orderPrice"];//市场价格
    [Ntalker ntalker_uploadTrailWithPageName:@"订单页" model:model];
}

- (void)Action_unReadCount:(NSDictionary *)params {
    void(^callback)(NSString * unread) = params[@"callback"];
    if (callback) {
       self.unreadCallback = callback;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unReadMessage:) name:NtalkerUnReadMessageNotification object:nil];
    NSArray *array = [Ntalker ntalker_getConsultHistoryListCount:100 offSet:0];
    if (array.count > 0) {
        NtalkerConsultRecord *recoder = array.firstObject;
        if (callback) {
          callback(recoder.unreadCount);
        }
    }
    if (callback) {
      callback(@"0");
    }
    
}

- (void)unReadMessage:(NSNotification *)not {
    [self onUnReadMessageWithDict:not.userInfo];
}
///未读消息处理
- (void)onUnReadMessageWithDict:(NSDictionary *)dict {
   // NSString *settingId = dict[@"settingId"];
    NSNumber *unreadCount = dict[@"unreadCount"];
    self.allUnreadCount = (NSInteger)unreadCount;
    if (self.unreadCallback) {
        self.unreadCallback([NSString stringWithFormat:@"%@",unreadCount]);
    }
}
//MARK:delegate
- (UIView *)ntalker_reSetProductInfoViewWithGoodsInfo:(NTalkerChatTableHeaderModel *)model cell:(UITableViewCell *)cell sendGoodsInfoSelector:(SEL)action {
    UIView *view = [self layoutProductItemView:model];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (UIView *)layoutProductItemView:(NTalkerChatTableHeaderModel *)model {
    UIView *view = [UIView new];
    view.clipsToBounds = YES;
    view.layer.cornerRadius = 8;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 8;
    [view addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(imageView.mas_height);
        make.centerY.mas_equalTo(view);
        make.left.mas_equalTo(view).offset(12);
        make.top.equalTo(view).offset(12);
        
    }];
    [imageView sd_setImageWithURL:[NSURL URLWithString:model.iu]];
    UILabel *titleLab = [UILabel new];
    titleLab.text = model.pn;
    titleLab.font = [UIFont systemFontOfSize:15];
    titleLab.textColor = [UIColor colorWithRed:20.f/255.f green:20.f/255.f  blue:20.f/255.f  alpha:1];
    titleLab.numberOfLines = 2;
    [view addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView.mas_right).offset(8);
        make.right.mas_equalTo(view.mas_right).offset(-12);
        make.top.mas_equalTo(imageView);
    }];
    
    UILabel *priceLab = [UILabel new];
    priceLab.text = [NSString stringWithFormat:@"¥%@",model.sp];
    priceLab.font = [UIFont systemFontOfSize:15];
    priceLab.textColor = [UIColor redColor];
    priceLab.numberOfLines = 1;
    [view addSubview:priceLab];
    [priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView.mas_right).offset(8);
        make.bottom.mas_equalTo(imageView);
    }];
    
    return view;
    
}

@end
