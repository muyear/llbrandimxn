//
//  AppDelegate.h
//  LLBrandIMXN
//
//  Created by muyear on 2018/12/18.
//  Copyright © 2018 muyear. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

